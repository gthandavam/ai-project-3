%helper to get all successors of a node but the goal node
getSuccessors(X, List, Visited):-
    first(X,Node),
    third(X,Cx),
    fourth(X,Px),
    Cost is Cx + 1,
    bagof([Z,HCost,Cost,P|Px], ( edge(Node,Z,P,_), \+member(Z,Visited), 
    goal(G), manhattan(G,Z, H), HCost is Cost + H),List).

%manhattan heuristic
manhattan((X,Y),(U,V), Ret):-
    Ret is abs(X-U) + abs(Y-V).

%to join two lists
append([L1|L4],L2,[L1|L3]) :- append(L4,L2,L3).
append([],L1,L1).

%list membership test for an element
member(H,[H|_]).
member(H,[_|T]) :- member(H,T).

first([E|_],E).
second([_,E|_],E).
third([_,_,E|_],E).
fourth([_,_,_|E],E).

%remaining fring is empty
pQ([],X,X).

%no successors
pQ(X,[],X).

pQ([H|Rest],X,Ret):-
    second(H,Hcost),
    first(X,X1),
    second(X1,Xcost),   
    Hcost >  Xcost, pQ([],[X1,H|Rest],Ret).

pQ([H|Rest],X,[H|Ret1]):-
    second(H,Hcost),
    first(X,X1),
    second(X1,Xcost),   
    Hcost =< Xcost, pQ(Rest,X,Ret1).

%pQ for performing insertion sort for one element
%pQA for performing insertion sort for A List
pQA(Fringe, [], Fringe).

pQA(Fringe, [H|Rest], Ret):-
    pQ(Fringe,[H],TRet),
    pQA(TRet,  Rest, Ret).

visitedAdd(Visited, X , VisitedNew) :-
	(\+member(X,Visited),
	VisitedNew = [X|Visited]);
	(member(X,Visited),
	VisitedNew = Visited).

findPath([Head|_], Goal, _, Path, Cost):-
	first(Head,Node),
	Node == Goal,
	third(Head, Cost),
	fourth(Head,Path).

findPath([Head|Rest], Goal, Visited, Path, Cost):-
	%Node -> is the x,y
	first(Head,Node),
	(getSuccessors(Head, List, Visited); List = []),
	pQA(Rest,List,Fringe),
	%append(Rest,List,Fringe),
	visitedAdd(Visited, Node, VisitedNew),
	findPath(Fringe, Goal, VisitedNew, Path, Cost), !.
