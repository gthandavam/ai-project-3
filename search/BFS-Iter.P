%helper to get all successors of a node but the goal node
getSuccessors(X, List, Visited):-
	first(X,Node),
	second(X,Cx),
	third(X,Px),
	Cost is 0,
	bagof([Z,Cost,P|Px], (edge(Node,Z,P,C), \+member(Z,Visited)),List).
	



%to join two lists
append([L1|L4],L2,[L1|L3]) :- append(L4,L2,L3).
append([],L1,L1).

%list membership test for an element
member(H,[H|_]).
member(H,[_|T]) :- member(H,T).

first([E|_],E).
second([_,E|_],E).	
third([_,_|E],E).
third2([_,_,E|_],E).

visitedAdd(Visited, X , VisitedNew) :-
	(\+member(X,Visited),
	VisitedNew = [X|Visited]);
	(member(X,Visited),
	VisitedNew = Visited).


findPath([Head|_], Goal, _, Path, Cost):-
	first(Head,Node),
	Node == Goal,
	second(Head, Cost),
	third(Head,Path).


findPath([Head|Rest], Goal, Visited, Path, Cost):-
	%Node -> is the x,y 
	first(Head,Node),
	(getSuccessors(Head, List, Visited); List = []),
	append(Rest,List,Fringe),
	visitedAdd(Visited, Node, VisitedNew),
	findPath(Fringe, Goal, VisitedNew, Path, Cost), !.
