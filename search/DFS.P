%findPath(X,Y, Path, Cost) :-
%	not goal(X),
%	edge(X,Y, Direction, Cost),	
%	Path is [Path, Direction, Path|Direction], %check concatenation
%	Cost is Cost + 1.

%findPath(X,Y, Path, Cost) :-
%	X == Y,
%	Path = 'Stop',
%	Cost is 0.
%
%findPath(X,Y, Path, Cost) :-
%	edge(X,Y,Direction,EdgeCost),
%	Path = 'Stop',
%	Path is Direction,
%	Cost is EdgeCost.
%	Cost is 0.
%
%findPath(X,Y, Path, Cost) :-
%	findPath(X,Z, PathZ, CostZ), %optimize edge(X,Z) ?
%   findPath(Z,Y, PathY, CostY),
%	Path is [PathZ, PathY, PathZ|PathY], %check concatenation
%	Cost is CostZ + CostY.

member(H,[H|_]).
member(H,[_|T]) :- member(H,T).

%Unable to run it for 2d maze
connected(X,Y,Visited,Path,Cost) :- 
	edge(X,Y,P,C), 
	\+member(Y,Visited),
	%Visited = [Y|Visited],
	Path = [P], 
	Cost is C.
	%edge(Y,X,P1,C1), Path = P1, Cost is C1.
	%easier to make the KB contain all the edges

connected(X,Y,Visited,Path,Cost) :- 
	edge(X,Z,Pe,Ce), 
	Z \== Y,
	\+member(Z,Visited),
	%Order of Z|Visited important in concatenation
	%Visited = [Z|Visited],
	%connected(Z,Y,Visited,P,C),
	connected(Z,Y,[Z|Visited],P,C),
	Path = [Pe | P],
	Cost is Ce + C.
	
